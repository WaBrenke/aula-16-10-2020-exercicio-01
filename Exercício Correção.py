
#Tuplas com respostas do joao (questão, alternativa)
joao = [
    (1,'C'),
    (2,'A'),
    (3,'D'),
    (4,'B'),
    (5,'D')
]

#Tuplas com respostas do maria (questão, alternativa)
maria = [
    (1,'C'),
    (2,'D'),
    (3,'D'),
    (4,'D'),
    (5,'A')
]

#Tuplas com respostas corretas e pesos (questão, alternativa, peso)
gabarito = [
    (1,'A',3),
    (2,'A',2),
    (3,'B',1),
    (4,'A',1),
    (5,'C',3)
]

#Notas finais
notaJoao = 0
notaMaria = 0

#Verifica quais questões estão corretas
for i in range(5):

    #Se a questão estiver correta, adiciona o peso na nota final
    if joao[i] == gabarito[i][0:2]:
        notaJoao = notaJoao + gabarito[i][2]

    #Se a questão estiver correta, adiciona o peso na nota final
    if maria[i] == gabarito[i][0:2]:
        notaMaria = notaMaria + gabarito[i][2]


#Imprime as notas finais

print(f"A nota do João foi {notaJoao}")
print(f"A nota da Maria foi {notaMaria}")

