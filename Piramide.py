
#Nova lista de funcionarios
total=[]
#Lista de funcionarios
funcionarios=[
        {"Nome":"Bruno","Vendas":0,"Equipe":["Ana","Diandra","Nico"]},
        {"Nome":"Ana","Vendas":300,"Equipe":["Bia","Camila"]},
        {"Nome":"Diandra","Vendas":450,"Equipe":["Maga","Fábio"]},
        {"Nome":"Nico","Vendas":500,"Equipe":["Lucas","Vini"]},
        {"Nome":"Bia","Vendas":100},
        {"Nome":"Camila","Vendas":350},
        {"Nome":"Maga","Vendas":200},
        {"Nome":"Fábio","Vendas":210},
        {"Nome":"Lucas","Vendas":150},
        {"Nome":"Vini","Vendas":100},
]

#Inverter a ordem da lista
funcionarios.reverse()

#Para cada funcionario em funcionarios
for funcionario in funcionarios:

    #Inicializa total de vendas
    valorVendas=0

    #Se o funcionario é lider
    if funcionario.get('Equipe') != None:

        #Para cada um dos subordinados do lider que são funcionarios
        #Isso daqui pq não da pra acessar listas usando indice de dicionario,
        for subordinado in funcionarios:
            if subordinado["Nome"] in funcionario["Equipe"]:
                #Calcula os 5% do valor de vendas dos subordinados na equipe e soma
                valorVendas = valorVendas + (0.05*subordinado["Vendas"])

        #Adiciona o valor calculado ao total de vendas do lider
        funcionario["Vendas"] = funcionario["Vendas"] + valorVendas

    #Atualiza a lista de funcionarios
    total.append(funcionario)


#Volta a lista na ordem original
total.reverse()
#Printa totais
for func in total:
    print(f"O total ganho por {func['Nome']} foi de: R${func['Vendas']}")



